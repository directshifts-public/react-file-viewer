// Copyright (c) 2017 PlanGrid, Inc.

import React, { Component } from 'react';

import 'styles/photo-viewer.scss';

const ZOOM_STEP = 40;

export default class PhotoViewer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      width: props.width,
      height: props.height
    };

    this.canvasRef = React.createRef();

    this.getImageDimensions = this.getImageDimensions.bind(this);
    this.updateImageDimensions = this.updateImageDimensions.bind(this);
    this.increaseZoom = this.increaseZoom.bind(this);
    this.reduceZoom = this.reduceZoom.bind(this);
    this.resetZoom = this.resetZoom.bind(this);
  }

  componentDidMount() {
    const imageDimensions = this.getImageDimensions();
    this.updateImageDimensions(imageDimensions.width, imageDimensions.height);
    this.setState(imageDimensions);
  }

  componentDidUpdate() {
    const { width, height } = this.state;
    this.updateImageDimensions(width, height);
  }

  getImageDimensions() {
    const { originalWidth, originalHeight } = this.props;
    const { height: viewerHeight, width: viewerWidth } = this.props;
    let imgHeight;
    let imgWidth;

    // Scale image to fit into viewer
    if (originalHeight <= viewerHeight && originalWidth <= viewerWidth) {
      imgWidth = originalWidth;
      imgHeight = originalHeight;
    } else {
      const heightRatio = viewerHeight / originalHeight;
      const widthRatio = viewerWidth / originalWidth;
      if (heightRatio < widthRatio) {
        imgHeight = originalHeight * heightRatio;
        imgWidth = originalWidth * heightRatio;
      } else {
        imgHeight = originalHeight * widthRatio;
        imgWidth = originalWidth * widthRatio;
      }
    }

    return { height: imgHeight, width: imgWidth };
  }

  updateImageDimensions(width, height) {
    let image = this.props.texture.image;
    image.style.width = `${width}px`;
    image.style.height = `${height}px`;
    image.setAttribute('class', 'photo');
    this.canvasRef.current.appendChild(image);
  }

  increaseZoom() {
    const { width, height } = this.state;
    this.setState({
      width: width + ZOOM_STEP,
      height: height + ZOOM_STEP
    });
  }

  reduceZoom() {
    const { width, height } = this.state;
    if (width === 1 || height === 1) return;
    this.setState({
      width: width - ZOOM_STEP,
      height: height - ZOOM_STEP
    });
  }

  resetZoom() {
    let { height, width } = this.getImageDimensions();
    this.setState({
      width: width,
      height: height
    });
  }

  render() {
    const containerStyle = {
      width: `${this.props.width}px`,
      height: `${this.props.height}px`,
      display: 'block'
    };

    return (
      <div className="photo-viewer-container" id="pg-photo-container" style={containerStyle}>
        <div className="viewer photo-viewer">
          <div className='photo-canvas' ref={this.canvasRef} />
        </div>
      </div>
    );
  }
}
