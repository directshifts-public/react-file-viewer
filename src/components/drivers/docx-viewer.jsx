// Copyright (c) 2017 PlanGrid, Inc.

import React, { Component } from 'react';
import mammoth from 'mammoth';

import 'styles/docx.scss';
import Loading from '../loading';

const DEFAULT_FONT_SIZE = 16;

export default class extends Component {
  constructor(props) {
    super(props);
    this.state = {
      document: null,
      fontSize: DEFAULT_FONT_SIZE
    };

    this.canvasRef = React.createRef();

    this.updateViewer = this.updateViewer.bind(this);
    this.increaseZoom = this.increaseZoom.bind(this);
    this.reduceZoom = this.reduceZoom.bind(this);
    this.resetZoom = this.resetZoom.bind(this);
  }

  componentDidMount() {
    const jsonFile = new XMLHttpRequest();
    jsonFile.open('GET', this.props.filePath, true);
    jsonFile.send();
    jsonFile.responseType = 'arraybuffer';
    jsonFile.onreadystatechange = () => {
      if (jsonFile.readyState === 4 && jsonFile.status === 200) {
        mammoth.convertToHtml(
          { arrayBuffer: jsonFile.response },
          { includeDefaultStyleMap: true },
        )
        .then((result) => {
          this.setState({ document: result });
        })
        .catch((a) => {
          console.log('alexei: something went wrong', a);
        })
        .done();
      }
    };
  }

  componentDidUpdate() {
    this.updateViewer();
  }

  updateViewer() {
    const { document } = this.state;
    if (document) this.canvasRef.current.innerHTML = document.value;
  }

  increaseZoom() {
    const { fontSize } = this.state;
    this.setState({ fontSize: fontSize + 1});
  }

  reduceZoom() {
    const { fontSize } = this.state;
    if (fontSize === 1) return;
    this.setState({ fontSize: fontSize - 1});
  }

  resetZoom() {
    this.setState({ fontSize: DEFAULT_FONT_SIZE });
  }

  render() {
    const { fontSize } = this.state;

    return (
      <div className="docx-viewer-container">
        <div className="viewer docx-viewer">
          <div className='docx-canvas' ref={this.canvasRef} style={{ fontSize: fontSize }}>
            <Loading/>
          </div>
        </div>
      </div>
    );
  }
}
