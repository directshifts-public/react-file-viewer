// High DPI crisp display pdf page rendering solution provided by lemycanh
// https://stackoverflow.com/questions/76495326/pdfjs-rendering-is-blurry-not-same-as-viewer-html/76503590#76503590
// Solution: https://jsfiddle.net/f4opuvLm/

import React, { useEffect, useRef } from 'react';
import PropTypes from 'prop-types';

const DEFAULT_SCALE = 1.1;
// The MAX_CANVAS_PIXELS constraint ensures the canvas size doesn’t exceed the browser's capabilities,
// avoiding rendering errors or crashes.
// 4k pixel
const MAX_CANVAS_PIXELS = 16777216;

// Determines the scaling factor based on the device's pixel ratio,
// which improves rendering quality on high-DPI screens
class OutputScale {
  constructor() {
    // Pixel ratio is typically 1 for standard displays, higher for high-DPI displays
    const pixelRatio = window.devicePixelRatio || 1;
    this.sx = pixelRatio;
    this.sy = pixelRatio;
  }

  get scaled() {
    return this.sx !== 1 || this.sy !== 1;
  }
}

// Approximates a floating-point number x as a fraction [numerator, denominator]
// using a Farey sequence with a limit of 8.
function approximateFraction(x) {
  // Fast paths for int numbers or their inversions.
  if (Math.floor(x) === x) {
    return [x, 1];
  }
  const xinv = 1 / x;
  const limit = 8;
  if (xinv > limit) {
    return [1, limit];
  } else if (Math.floor(xinv) === xinv) {
    return [1, xinv];
  }

  const x_ = x > 1 ? xinv : x;
  // a/b and c/d are neighbours in Farey sequence.
  let a = 0,
    b = 1,
    c = 1,
    d = 1;
  // Limiting search to order 8.
  while (true) {
    // Generating next term in sequence (order of q).
    const p = a + c,
      q = b + d;
    if (q > limit) {
      break;
    }
    if (x_ <= p / q) {
      c = p;
      d = q;
    } else {
      a = p;
      b = q;
    }
  }
  let result;
  // Select closest of the neighbours to x.
  if (x_ - a / b < c / d - x_) {
    result = x_ === x ? [a, b] : [b, a];
  } else {
    result = x_ === x ? [c, d] : [d, c];
  }
  return result;
}

// Ensures the canvas dimensions are aligned with the fractional scaling factors for better rendering precision.
function roundToDivide(x, div) {
  const r = x % div;
  return r === 0 ? x : Math.round(x - r + div);
}

const PdfPage = (
  {
    pdf,
    pageNum,
    containerWidth = 670,
    zoomLevel = 0
  }
) => {
  const canvasRef = useRef(null);
  const renderTaskRef = useRef(null);

  useEffect(() => {
    pdf.getPage(pageNum).then(async (page) => {
      const calculatedScale = (containerWidth / page.getViewport({ scale: DEFAULT_SCALE }).width);
      const scale = calculatedScale > DEFAULT_SCALE ? DEFAULT_SCALE : calculatedScale;
      const viewport = page.getViewport({ scale: scale + zoomLevel });
      const { width, height } = viewport;

      const canvas = canvasRef.current;
      const context = canvas.getContext('2d', { alpha: false });

      // Adjusts the canvas dimensions and transform matrix for accurate rendering on different devices.
      // adjusts for the device's pixel ratio.
      const outputScale = new OutputScale();
      const pixelsInViewport = width * height;
      const maxScale = Math.sqrt(MAX_CANVAS_PIXELS / pixelsInViewport);
      if (outputScale.sx > maxScale || outputScale.sy > maxScale) {
        outputScale.sx = maxScale;
        outputScale.sy = maxScale;
      }

      // Ensure alignments
      const sfx = approximateFraction(outputScale.sx);
      const sfy = approximateFraction(outputScale.sy);

      canvas.width = roundToDivide(width * outputScale.sx, sfx[0]);
      canvas.height = roundToDivide(height * outputScale.sy, sfy[0]);

      const { style } = canvas;
      style.width = roundToDivide(width, sfx[1]) + "px";
      style.height = roundToDivide(height, sfy[1]) + "px";

      const transform = outputScale.scaled
        ? [outputScale.sx, 0, 0, outputScale.sy, 0, 0]
        : null;

      if (renderTaskRef.current) {
        renderTaskRef.current.cancel();
      }

      renderTaskRef.current = page.render({
        canvasContext: context,
        transform,
        viewport,
      });

      try {
        await renderTaskRef.current.promise;
      } catch (error) {
        if (error.name !== 'RenderingCancelledException') {
          throw error
        }
      }
    });
  }, [pdf, pageNum, zoomLevel, containerWidth]);

  return (
    <div key={`page-${pageNum}`} className='pdf-canvas'>
      <canvas ref={canvasRef} width='670' height='870'/>
    </div>
  );
};

PdfPage.propTypes = {
  containerWidth: PropTypes.number,
  pdf: PropTypes.instanceOf(Object).isRequired,
  pageNum: PropTypes.number.isRequired,
  zoomLevel: PropTypes.number,
};

export default PdfPage;
