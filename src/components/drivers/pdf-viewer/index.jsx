import React, { forwardRef, useEffect, useImperativeHandle, useState } from 'react';
import { getDocument, GlobalWorkerOptions } from 'pdfjs-dist';
import PropTypes from 'prop-types';
import PdfPage from './PdfPage';

GlobalWorkerOptions.workerSrc = 'https://cdn.jsdelivr.net/npm/pdfjs-dist@4.9.155/build/pdf.worker.min.mjs';
const INCREASE_PERCENTAGE = 0.2;

const PDFViewer = forwardRef(({ filePath }, driverRef) => {
  const [pdfDoc, setPdfDoc] = useState(null);
  const [zoomLevel, setZoomLevel] = useState(0);
  const [containerWidth, setContainerWidth] = useState(0);
  const [loadingProgress, setLoadingProgress] = useState(0);
  const [errorMessage, setErrorMessage] = useState(null)

  const container = document.getElementById('pdf_viewer');

  useEffect(() => {
    setContainerWidth(container?.offsetWidth ?? 0);
  }, [container?.offsetWidth]);

  useEffect(() => {
    if (filePath) {
      const loadingTask = getDocument(filePath);

      loadingTask.onProgress = (progressData) => {
        const { loaded, total } = progressData;
        setLoadingProgress((loaded / total) * 100);
      };

      loadingTask.promise
        .then((pdf) => setPdfDoc(pdf))
        .catch((error) => {
          if (error.name === 'PasswordException') {
            setErrorMessage('Unable to open the password protected file')
          } else {
            throw error;
          }
        });
    }
  }, [filePath]);

  useImperativeHandle(driverRef, () => ({
    increaseZoom: () => setZoomLevel(zoomLevel + 1),
    reduceZoom: () => setZoomLevel(zoomLevel - 1),
    resetZoom: () => setZoomLevel(0)
  }));

  const renderPages = () => {
    const pdfPages = [];
    for(let index = 0; index < pdfDoc.numPages; index++) {
      const pageNum = index + 1;
      pdfPages.push(
        (
          <PdfPage key={`pdf-page-${pageNum}`}
                   containerWidth={containerWidth} pdf={pdfDoc} pageNum={pageNum} zoomLevel={zoomLevel * INCREASE_PERCENTAGE}/>
        )
      );
    }

    return pdfPages;
  }

  return (
    <div className="pdf-viewer-container">
      <div className='viewer pdf-viewer' id='pdf_viewer'>
        {pdfDoc ? renderPages() : (
          <p style={{ textAlign: 'center' }}>
            {errorMessage ?? `Loading PDF... ${Math.round(loadingProgress)}%`}
          </p>
        )}
      </div>
    </div>
  )
});

PDFViewer.propTypes = {
  filePath: PropTypes.string.isRequired,
};

export default PDFViewer;
