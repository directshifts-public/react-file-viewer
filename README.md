# react-file-viewer

Extendable file viewer for web

## Supported file formats:

 - Images: png, jpeg, gif, bmp, including 360-degree images
 - pdf
 - csv
 - xslx
 - docx
 - Video: mp4, webm
 - Audio: mp3


## Usage

Note this module works best with react 16+.  If you are using React < 16 you will likely need to use version 0.5. `npm install react-file-viewer@0.5.0`.

There is one main React component, `FileViewer`, that takes the following props:

`fileType` string: type of resource to be shown (one of the supported file
formats, eg `'png'`). Passing in an unsupported file type will result in displaying
an `unsupported file type` message (or a custom component).

`filePath` string: the url of the resource to be shown by the FileViewer.

`onError` function [optional]: function that will be called when there is an error in the file
viewer fetching or rendering the requested resource. This is a place where you can
pass a callback for a logging utility.

`errorComponent` react element [optional]: A component to render in case of error
instead of the default error component that comes packaged with react-file-viewer.

`unsupportedComponent` react element [optional]: A component to render in case
the file format is not supported.

To use a custom error component, you might do the following:

```
// MyApp.js
import React, { Component } from 'react';
import logger from 'logging-library';
import FileViewer from 'react-file-viewer';
import { CustomErrorComponent } from 'custom-error';

const file = 'http://example.com/image.png'
const type = 'png'

class MyComponent extends Component {
  render() {
    return (
      <FileViewer
        fileType={type}
        filePath={file}
        errorComponent={CustomErrorComponent}
        onError={this.onError}/>
    );
  }

  onError(e) {
    logger.logError(e, 'error in file-viewer');
  }
}
```

## Development

There is a demo app built into this library that can be used for development
purposes. It is by default served via webpack-dev-server.

### To start demo app

`make start` will start the demo app served by webpack-dev-server

### Testing

Tests use Jest and Enzyme.

Run tests with:

```
make test
```

This starts Jest in watch mode. To run a particular test file, while in watch mode
hit `p` and then type the path or name of the file.

Some tests use snapshots. If intended changes to a component cause snapshot tests
to fail, snapshot files need to be updated (stored in `__snapshots__` directories).
To do this run:

```
npm run jest --updateSnapshot
```

### To run the linter

`make lint`

### Extending the file viewer

Adding supported file types is easy (and pull requests are welcome!). Say, for
example, you want to add support for `.rtf` files. First, you need to create a
"driver" for that file type. A driver is just a component that is capable of
rendering that file type. (See what exists now in `src/components/drivers`.) After
you've created the driver component and added it to `src/components/drivers`, you
simply need to import the component into `file-vewer.jsx` and add a switch clause
for `rtf` to the `getDriver` method. Ie:

```
case 'rtf':
  return RtfViewer;
```

## Roadmap

- Remove ignored linting rules and fix them

## How to make changes

This package is not published, so currently we're using the compiled file directly from this repo.
This means that the npm will use the compiled file stored in `dist/index.js` for this package directly.
Follow these instructions to make changes:

- Make the required changes
- Run `npm run build` (This will generate the compiled file in `dist/index.js`)
- Push the changes along with `dist/index.js`
- Run `yarn upgrade react-file-viewer --latest` to in your project to fetch the latest version

# Building Individual Packages

## Overview
- Running `yarn build` creates a single file containing all supported file viewers.
    - **Note**: A combined build increases the file size.
- If you intend to use it for a single file type, you can build a package that includes only the specific file viewer (e.g., PDFViewer or DocxViewer).

---

## 1: Building a PDF Viewer

PDFViewer is built using Mozilla's [pdf.js](https://github.com/mozilla/pdf.js/wiki/Setup-pdf.js-in-a-website) package, which consists of:
- **Core library**: `pdf.js`
- **Worker file**: `pdf.worker.js`

> Using a CDN for the worker file is recommended to reduce bundle size. Including both core and worker files in a single bundle can increase the bundle size over 2 MB.

### Steps to Build PDFViewer

1. **Modify the Entry Point**
    - The module entry point is **FileViewer**.
    - In the `getDriver` method:
        - Comment out all viewers except PDFViewer.
    - Export only PDFViewer in `src/components/drivers/index.js`.

2. **Customize the PDFViewer (Optional)**
    - Update the **PDFViewer** component in the `src/drivers/pdf-viewer` directory.

3. **Test Your Changes**
    - Run `yarn start` to start the development server.
        - The **app.js** will call the **FileViewer** component.
        - Pass a file from the `example_files` directory to test your changes in **app.js**.

4. **Prepare for Production**
    - Disable minification for the `pdfjs-dist` package in the **webpack.config.js** file.
        - **Note**: Minification won’t cause build errors but may result in runtime "Critical dependency the request of a dependency is an expression" warnings.
            Component may brake during run time.
          - pdfjs library will not work correctly if the minifier did not keep original class/function names intact
            Reference: https://github.com/mozilla/pdf.js/tree/master/examples/webpack
          - pdfjs-dist uses dynamic imports and build using ESM modules which needs a separate babel loader to convert it to UMD
          - It is better not to minimize pdfjs-dist and keep it as it is until we add babel loader to handle dynamic imports

5. **Build the Package**
    - Run `yarn build` to export the PDFViewer.
    - The compiled package (`index.js`) will be available in the **dist** directory.

## 2. DocxViewer Build:

- In `src/components/drivers/index.js`:
   - Uncomment `export default DocxViewer`.
   - Comment out `export default PDFViewer`, `UnsupportedViewer`, `PhotoViewer`, and `PhotoViewerWrapper`.

- In `src/components/drivers/file-viewer.jsx`:
   - Uncomment `import DocxViewer` and its associated switch case.
   - Comment out imports for `PDFViewer`, `PhotoViewerWrapper`, `UnsupportedViewer`, and their respective switch cases.

- Execute `yarn build`. The compiled code will only contain `DocxViewer`.

## 3. PhotoViewer Build:

- In `src/components/drivers/index.js`:
   - Uncomment `export default PhotoViewer` and `PhotoViewerWrapper`.
   - Comment out `export default PDFViewer`, `DocxViewer`, and `UnsupportedViewer`.

- In `src/components/drivers/file-viewer.jsx`:
   - Uncomment `import PhotoViewerWrapper` and its associated switch case.
   - Comment out imports for `PDFViewer`, `DocxViewer`, `UnsupportedViewer`, and their respective switch cases.

- Execute `yarn build`. The compiled code will only contain `PhotoViewer`.

## 4. UnsupportedViewer Build:

- In `src/components/drivers/index.js`:
   - Uncomment `export default UnsupportedViewer`.
   - Comment out `export default PDFViewer`, `DocxViewer`, `PhotoViewer`, and `PhotoViewerWrapper`.

- In `src/components/drivers/file-viewer.jsx`:
   - Uncomment `import UnsupportedViewer` and its associated switch case.
   - Comment out imports for `PDFViewer`, `DocxViewer`, `PhotoViewerWrapper`, and their respective switch cases.

- Execute `yarn build`. The compiled code will only contain `UnsupportedViewer`.
