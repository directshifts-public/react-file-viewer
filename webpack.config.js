// Copyright (c) 2017 PlanGrid, Inc.

const path = require('path');
const webpack = require('webpack');

const BUILD_DIR = path.resolve(__dirname, './dist');
const APP_DIR = path.resolve(__dirname, './src');

const config = {
  mode: 'production',
  entry: `${APP_DIR}/components`,
  output: {
    path: BUILD_DIR,
    filename: 'index.js',
    library: {
      name: 'FileViewer',
      type: 'umd',
    },
    clean: true,
  },
  resolve: {
    modules: [path.resolve(__dirname, './src'), 'node_modules'],
    extensions: ['.js', '.jsx', '.json'],
    alias: { 'pdfjs-dist': path.resolve(__dirname, 'node_modules/pdfjs-dist') },
  },
  experiments: {
    topLevelAwait: true,  // if using top-level await (optional)
    asyncWebAssembly: true, // if using WebAssembly in your PDF viewer (optional)
  },
  externals: [
    {
      react: {
        root: 'React',
        commonjs2: 'react',
        commonjs: 'react',
        amd: 'react',
      },
    },
    {
      'react-dom': {
        root: 'ReactDOM',
        commonjs2: 'react-dom',
        commonjs: 'react-dom',
        amd: 'react-dom',
      },
    },
  ],
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        include: path.resolve(__dirname, './src'),
        loader: 'babel-loader',
        options: {
          cacheDirectory: true,
        }
      },
      {
        test: /\.(css|scss)$/,
        use: ['style-loader', 'css-loader', 'sass-loader', 'postcss-loader'],
      },
      {
        test: /\.(png|jpg|gif|svg)$/,
        type: 'asset/resource',
      },
    ]
  },
  plugins: [
    new webpack.ContextReplacementPlugin(
      /pdfjs-dist[\/\\]build|pdfjs-dist[\/\\]webpack/,
      path.resolve(__dirname, 'node_modules/pdfjs-dist')
    ),
  ],
  optimization: {
    // PDFViewer is developed using pdfjs library.
    // pdfjs library will not work correctly if the minifier did not keep original class/function names intact
    // Reference: https://github.com/mozilla/pdf.js/tree/master/examples/webpack
    // Disable the minifier only for pdfjs-dist
    // pdfjs-dist uses dynamic imports which needs a separate babel loader to handle it.
    // It is better not to minimize pdfjs-dist and keep it as it is until we add babel loader to handle dynamic imports
    minimize: true
  },
}

module.exports = config;
