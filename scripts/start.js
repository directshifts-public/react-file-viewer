// Copyright (c) 2017 PlanGrid, Inc.

process.env.NODE_ENV = 'development';
process.on('unhandledRejection', err => {
  throw err;
});

const PORT = 8081;

const webpack = require('webpack');
const WebpackDevServer = require('webpack-dev-server');
const config = require('../webpack.config.dev.js');

const compiler = webpack(config);

const devServerOptions = {
  hot: true,
  port: PORT,
  static: {
    directory: config.output.path, // Serves files from the output directory
  },
  devMiddleware: {
    publicPath: config.output.publicPath,
  },
  client: {
    overlay: true, // Displays errors and warnings in the browser
  },
  watchFiles: {
    paths: ['src/**/*'], // Watch source files for changes
    options: {
      ignored: /node_modules/,
    },
  },
};

// Create and start the DevServer
const devServer = new WebpackDevServer(devServerOptions, compiler);

devServer.startCallback(() => {
  console.log(`Development server listening on port ${PORT}`);
});
